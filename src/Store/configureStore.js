import { createStore, applyMiddleware } from 'redux';

// It receives reducer and middlewares and returns a store.
function generateStore( reducer, middleware = [] ){
    if( reducer ) {
        return createStore( reducer, applyMiddleware( ...middleware ) );
    }
}

export default generateStore;