import API from '../Axios';
import {
    GET_PRODUCTS,
    ADD_ORDER,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    EDIT_ORDER,
    PRODUCT_DETAILS
} from './actionTypes';

export function getProducts( products ) {
    return { type: GET_PRODUCTS, products };
}

export function getAllProducts() {
    
    return API.get( 'e62c6c2b-d4e1-462f-a1e4-859dc311c66f' );
}

export function productDetails( product ) {
    return { type: PRODUCT_DETAILS, product };
}

export function addOrder( orders, user, sourcePath ) {
    user.orders = [ ...user.orders, ...orders ];
    if ( sourcePath === `/cart` ) {
        user.cart = [];
    }
    return {
        type: ADD_ORDER,
        user,
        orders
    }
}

export function editOrder ( userOrder, user, orderIndex ){
    const temp = user.orders.map ( ( order, index ) => {
        if ( index === orderIndex ) {
            return userOrder;
        }
        else{
            return order;
        }
    } );
    user.orders = temp;
    return {
        type: EDIT_ORDER,
        user,
        userOrder
    };
}

export function addProductToCart ( product, user ) {
    const [ flag ] = user.cart.filter( ( cartProduct ) => cartProduct.id === product.id );
    if ( ! flag ) {
        user.cart = [ ...user.cart, product ];
    }
    return {
        type: ADD_TO_CART,
        product,
        user,
    };
}

export function removeProductFromCart ( product, user ){
    const temp = user.cart.filter( ( cartProduct ) => cartProduct.id !== product.id );
    user.cart = temp;
    return {
        type: REMOVE_FROM_CART,
        user,
    };
}