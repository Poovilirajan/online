export const CREATE_USER = 'CREATE_USER';

export const CHANGE_USER = 'CHANGE_USER';

export const GET_PRODUCTS = 'GET_PRODUCTS';

export const ADD_ORDER = 'ADD_ORDER';

export const ADD_TO_CART = 'ADD_TO_CART';

export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';

export const EDIT_ORDER = 'EDIT_ORDER';

export const CREATE_SELLER = 'CREATE_SELLER';

export const LOG_OUT = 'LOG_OUT';

export const PRODUCT_DETAILS = 'PRODUCT_DETAILS';