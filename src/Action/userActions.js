import { CREATE_USER, CHANGE_USER, CREATE_SELLER, LOG_OUT } from './actionTypes'

export function createUser( data ) {
    const user = {
        firstname: data.firstname,
        lastname: data.lastname,
        mobile: data.mobile,
        password: data.password,
        addressline1: data.addressline1,
        addressline2: data.addressline2,
        state: data.state,
        city: data.city,
        pincode: data.pincode,
        orders: [],
        cart: []
    };
    
    return {
        type: CREATE_USER,
        user
    };
}

export function changeUser( user ) {

    return {
        type: CHANGE_USER,
        user
    };
}

export function createNewSeller ( data ) {
    const user = {
        firstname: data.firstname,
        lastname: data.lastname,
        mobile: data.mobile,
        password: data.password,
        mail: data.mail,
        accnumber: data.accnumber,
        products: data.products
    };

    return {
        type: CREATE_SELLER,
        user
    }
}

export function logOutUser () {

    return {
        type: LOG_OUT,
    };
}