export function getFilters( target, state ) {
    const { value, name } = target;
    let categories = state.categories;
    let { categoryFilter, priceFilter } = state;
    let { low : tempLow, high : tempHigh } = state;
    if ( target.checked ) {
        if ( name === 'category' ) {
            categories = [ ...categories, value ];
            categoryFilter = {
                ...categoryFilter,
                [ value ]: true
            }
        } else if ( name === 'price' ) {
            const [ low, high ] = ( value.split( '-' ) );
            if ( tempLow === 0 && tempHigh === Infinity ) {
                [ tempLow, tempHigh ] = [ low, high ];
            } else if ( tempHigh <= low ) {
                tempHigh = high;
            } else if ( tempLow >= low) {
                tempLow = low;
            }
            priceFilter = {
                ...priceFilter,
                [ value ]: true
            }
        }
    } else {
        if ( name === 'category' ) {
            let set = new Set ( categories );
            set.delete( value );
            categories = [...set];
            categoryFilter = {
                ...categoryFilter,
                [ value ]: false
            }
        } else if ( name === 'price' ) {
            const [ low, high ] = ( value.split( '-' ) );
            if ( tempHigh === high ) {
                tempHigh = low;
            } else if ( tempLow === low ) {
                tempLow = high;
            }
            priceFilter = {
                ...priceFilter,
                [ value ]: false
            }
        }
    }
    if ( tempLow === tempHigh ) {
        tempLow = 0;
        tempHigh = Infinity;
    }
    return {
        categories,
        categoryFilter,
        priceFilter,
        tempLow,
        tempHigh
    };
}

export function apply( state ) {
    let products = [];
    if ( state.categories.length !== 0 ) {
        products = state.products.filter ( ( product ) => {
            return state.categories.find( ( category ) => { 
                return product.category === category &&
                    product.price >=state.low &&
                    product.price <= state.high;
            } );
        } );
    } else {
        products = state.products.filter( ( product ) =>
            product.price >= state.low && product.price <= state.high );
    }
    return products;
}