export const AlPHABETS_ONLY_REGEX = /^[a-z]+$/i;

export const MOBILE_REGEX = /^\d{10}$/;

export const PASSWORD_REGEX = /^.{8}$/;

export const ADDRESS_REGEX = /.{3}/;

export const PINCODE_REGEX = /^\d{6}$/;

export const CARDNUMBER_REGEX = /^\d{4} \d{4} \d{4} \d{4}$/;

export const CVV_REGEX = /^\d{3}$/;

export const EMAIL_REGEX = /^.+\@.+\.\w{2,3}$/;

export const ACCOUNT_NUMBER_REGEX = /^\d{5,15}$/;