export const FIRST_NAME = 'firstname';

export const LAST_NAME = 'lastname';

export const MOBILE = 'mobile';

export const PASSWORD = 'password';

export const ADDRESS_LINE_1 = 'addressline1';

export const ADDRESS_LINE_2 = 'addressline2';

export const CITY = 'city';

export const STATE = 'state';

export const PINCODE = 'pincode';

export const CONFIRM_PASSWORD = 'confirmpass';

export const CARD_NUMBER = 'cardnumber';

export const CVV = 'cvv';

export const EMAIL = 'mail';

export const ACCOUNT_NUMBER = 'accnumber';

export const PRODUCTS = 'products';