export const CART = {
    products: [],
    removeFlag: false,
    bill: 0,
    ids: []
};

export const CREATE_ACCOUNT = {
    firstname: '',
    lastname: '',
    mobile: '',
    addressline1: '',
    addressline2: '',
    city: '',
    state: '',
    pincode: '',
    password: '',
    confirmpass: '',
    error: {
        firstnameError: ' ',
        lastnameError: ' ',
        mobileError: ' ',
        passwordError: ' ',
        addressline1Error: ' ',
        addressline2Error: '',
        cityError: ' ',
        stateError: ' ',
        pincodeError: ' ',
        confirmpassError: ' ',
    },
    stateList: [ 'Andhra Pradesh', 'Assam', 'Arunachal Pradesh', 'Bihar',
     'Goa', 'Gujarat', 'Jammu and Kashmir', 'Jharkhand', 'West Bengal',
     'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur',
     'Meghalaya', 'Mizoram', 'Nagaland', 'Orissa', 'Punjab', 'Rajasthan',
     'Sikkim', 'Tamil Nadu', 'Tripura', 'Uttaranchal', 'Uttar Pradesh',
     'Haryana', 'Himachal Pradesh', 'Chhattisgarh', 'Andaman and Nicobar', 
     'Pondicherry', 'Dadra and Nagar Haveli', 'Daman and Diu', 'Delhi',
      'Chandigarh', 'Lakshadweep' ],
};

export const DETAIL = {
    product: undefined,
    cartSuccess: false,
};

export const HOME = {
    products: [],
    filteredProducts: [],
    pageData:[],
    isFilter: false,
    showFilter: false,
    categoryFilter: { 'Mobiles' : false, 'Laptops' : false },
    priceFilter: { '10000-15000': false, '15000-20000': false },
    categories:[],
    high: Infinity,
    low: 0,
    offset: 0,
    perPage: 4,
    currentPage: 0
};

export const LOGIN = {
    mobile: '',
    password: '',
    error: { mobileError: ' ', passwordError: ' ' },
    loginError: false,
    disabled:true,
    isLogin:false
};

export const ORDER = {
    products: undefined,
    success: false,
    payment: '',
    cardnumber: undefined,
    count: [],
    cvv: '',
    creditCardDetail: false,
    debitCardDetail: false,
    error: { cardnumberError: ' ', cvvError: ' ' },
    bill: 0,
    disabled: true
};

export const ORDER_DETAIL = {
    order: undefined,
};

export const ORDER_LIST = {
    offset: 0,
    perPage:5,
    currentPage:0,
    pageData: []
};

export const SELLER = {
    firstname: '',
    lastname: '',
    mobile: '',
    password: '',
    confirmpass: '',
    mail: '',
    accnumber: '',
    products: [],
    error: {
        firstnameError: ' ',
        lastnameError: ' ',
        mobileError: ' ',
        passwordError: ' ',
        confirmpassError: ' ',
        mailError: ' ',
        accNumberError: ' ',
        productsError: ' '
    },
    success: false
}; 