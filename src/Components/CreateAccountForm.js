import { Alert, Button } from 'react-bootstrap';
import { FIRST_NAME, LAST_NAME, MOBILE, PASSWORD, ADDRESS_LINE_1, ADDRESS_LINE_2, CITY, STATE, PINCODE, CONFIRM_PASSWORD} from '../Utils/constants/inputNames'; 

function CreateAccountForm ( props ) {

    const error = props.state.error;
    const invalid = error.firstnameError || error.lastnameError ||
    error.mobileError || error.addressline1Error ||
    error.addressline2Error || error.cityError ||
    error.stateError || error.pincodeError ||
    error.passwordError || error.confirmpassError;   
    
    return (
        <form>
            <table id = 'userForm'>
                <tbody>
                    <tr>
                        <td colSpan = '2'><p id = 'formtitle'>Create User</p></td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Firstname</p>
                            <input
                                name = { FIRST_NAME }
                                value = { props.state.firstname }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput'
                                placeholder = 'Enter Firstname' />
                            { error.firstnameError && 
                                <Alert variant = 'danger' className = 'span'>
                                    { error.firstnameError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Lastname</p>
                            <input
                                name = { LAST_NAME }
                                value = { props.state.lastname }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput'
                                placeholder = 'Enter lastname' />
                            { error.lastnameError && 
                                <Alert variant = 'danger' className = 'span'>
                                    { error.lastnameError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Mobile</p>
                            <input 
                                id = 'countrycode'
                                name = 'countrycode'
                                value='+91' 
                                readOnly />
                            <input
                                name = { MOBILE }
                                value = { props.state.mobile }
                                onChange={ ( e ) => props.change( e ) }
                                id = 'mobileinput'
                                placeholder = 'Enter Mobile' />
                            { error.mobileError &&
                                <Alert variant = 'danger' className = 'span'>
                                    { error.mobileError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Address Line1</p>
                            <input
                                name = { ADDRESS_LINE_1 }
                                value = {props.state.addressline1}
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput' 
                                placeholder = 'Enter Address' />
                            { error.addressline1Error &&
                                <Alert variant = 'danger'className = 'span'> 
                                    { error.addressline1Error } 
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Address Line2 (optional)</p>
                            <input
                                name = { ADDRESS_LINE_2 }
                                value = { props.state.addressline2 }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput'
                                placeholder = 'Enter Address' />
                            { error.addressline2Error &&
                                <Alert variant = 'danger' className = 'span'>
                                    { error.addressline2Error }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>City</p>
                            <input
                                name = { CITY }
                                value = { props.state.city }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput'
                                placeholder = 'Enter City' />
                            { error.cityError &&
                                <Alert variant = 'danger' className = 'span'>
                                    { error.cityError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>State</p>
                            <select
                                name = { STATE }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput' >
                                <option value = '' > Select State </option>
                                { props.state.stateList.map( ( state, index ) =>
                                    <option key = { index } value = { state }>
                                        { state }
                                    </option> )
                                }
                            </select>
                            { error.stateError &&
                                <Alert variant = 'danger' className = 'span'>
                                    { error.stateError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Pincode</p>
                            <input
                                name = { PINCODE }
                                value = { props.state.pincode }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput'
                                placeholder = 'Enter Pincode' />
                            { error.pincodeError &&
                                <Alert variant = 'danger' className = 'span'>
                                    { error.pincodeError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Password</p>
                            <input
                                type = 'password'
                                name = { PASSWORD }
                                value = { props.state.password }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput'
                                placeholder = 'Enter Password' />
                            { error.passwordError &&
                                <Alert variant = 'danger' className = 'span'>
                                    { error.passwordError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Confirm Password</p>
                            <input
                                type = 'password'
                                name = { CONFIRM_PASSWORD }
                                value = { props.state.confirmpass }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput'
                                placeholder = 'Re-Enter Password' />
                            { error.confirmpassError &&
                                <Alert variant = 'danger' className = 'span'>
                                    { error.confirmpassError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td colSpan = '2'>
                            <Button
                                type = 'Submit'
                                id = 'formbutton'
                                onClick = { ( e ) => props.submit ( e ) }
                                disabled = { invalid } > Create </Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form> 
    );
}

export default CreateAccountForm;