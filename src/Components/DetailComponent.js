import { Alert, Button, Container, Table } from 'react-bootstrap';

function DetailUI ( props ) {
    const { product } = props.state;

    return (
        <Container>
            <Table id="viewtable">
                <tbody>
                    <tr>
                        <td colSpan = '2'>
                            <h2 id = 'productsheader'>
                                { product.brand + ' ' + product.name }
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img 
                                id = 'viewimage'
                                src = { product.ProductImage }
                                alt = { product.name } 
                            />
                        </td>
                        <td>
                            <div id='productdetail'>
                                <p>
                                    <strong> Product Name </strong> :
                                    { product.name } 
                                </p>
                                <p>
                                    <strong>Price </strong> : Rs.
                                    { product.price }/-
                                </p>
                                <p> <strong>Brand </strong> :
                                    { product.brand }
                                </p>
                                <p>
                                    <strong>Category </strong> :
                                    { product.category }
                                </p>
                                <p>
                                    <strong>Description </strong> :
                                    { product.desc }.
                                </p>
                            </div>
                            <p id = 'viewbutton' >
                                <Button
                                    variant = 'warning'
                                    className = 'viewbuttons'
                                    onClick = { () => props.order( product ) } >
                                    Buy Now
                                </Button>
                                <Button
                                    variant = 'warning'
                                    className = 'viewbuttons'
                                    onClick = { () => props.cart( product ) } >
                                    Add to Cart
                                </Button>
                            </p>
                            { props.state.cartSuccess &&
                                <Alert variant = 'success' > 
                                    <center> Product added To Cart </center>
                                </Alert>
                            }
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Container>
    );
}

export default DetailUI;