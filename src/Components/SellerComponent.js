import { Button, Alert } from "react-bootstrap";
import { FIRST_NAME, LAST_NAME, MOBILE, PASSWORD, CONFIRM_PASSWORD, EMAIL, ACCOUNT_NUMBER, PRODUCTS } from '../Utils/constants/inputNames';

function SellerComponent ( props ) {
    const error = props.state.error;
    const invalid = error.firstnameError || error.lastnameError ||
        error.mobileError || error.passwordError ||
        error.confirmpassError || error.mailError ||
        error.accNumberError || error.productsError ;
    return (
        <>
            { props.state.success 
            && <div id = 'success1'>
                <p id = 'success-message'>
                    Congratulations!<br/> Now You're a Seller!
                </p>
                <Button
                    variant = 'success'
                    id = 'success-button1'
                    onClick = { () => props.redirect() }>
                    Home
                </Button>
            </div>
            }
            <form>
                <table id = 'userForm'>
                    <tbody>
                        <tr>
                            <td>
                                <p id = 'formtitle'> Start Selling </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label'>Firstname</p>
                                <input 
                                    name = { FIRST_NAME }
                                    value = { props.state.firstname }
                                    onChange = { ( e ) => props.change ( e ) }
                                    className = 'forminput'
                                    placeholder = 'Enter Firstname' />
                                { error.firstnameError &&
                                    <Alert variant = 'danger' className = 'span'>
                                        { error.firstnameError }
                                    </Alert>
                                }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label'>Lastname</p>
                                <input
                                    name = { LAST_NAME }
                                    value = { props.state.lastname }
                                    onChange = { ( e ) => props.change ( e ) }
                                    className = 'forminput'
                                    placeholder = 'Enter lastname' />
                                { error.lastnameError &&
                                    <Alert variant = 'danger' className = 'span'>
                                        { error.lastnameError }
                                    </Alert>
                                }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label'>Mobile</p>
                                <input 
                                    id = 'countrycode'
                                    name = 'countrycode'
                                    value='+91' readOnly />
                                <input
                                    name = { MOBILE }
                                    value = { props.state.mobile }
                                    onChange = { ( e ) => props.change ( e ) }
                                    id = 'mobileinput'
                                    placeholder = 'Enter Mobile Number' />
                                { error.mobileError &&
                                    <Alert variant = 'danger' className = 'span'>
                                        { error.mobileError }
                                    </Alert>
                                }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label'> Business Mail </p>
                                <input
                                    name = { EMAIL }
                                    value = { props.state.mail }
                                    onChange = { ( e ) => props.change ( e ) }
                                    className = 'forminput'
                                    placeholder = 'Enter Business Mail' />
                                { error.mailError &&
                                    <Alert variant = 'danger' className = 'span'>
                                        { error.mailError }
                                    </Alert>
                                }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label'> Account Number </p>
                                <input
                                    name = { ACCOUNT_NUMBER }
                                    value = { props.state.accnumber }
                                    onChange = { ( e ) => props.change ( e ) }
                                    className = 'forminput'
                                    placeholder = 'Enter Account Number' />
                                { error.accNumberError &&
                                    <Alert variant = 'danger' className = 'span'>
                                        { error.accNumberError }
                                    </Alert>
                                }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label'> Products You Going to Sell </p>
                                <div id = 'products' >
                                    <p className = 'product-item'>
                                        <input 
                                            type = 'checkbox'
                                            name = { PRODUCTS }
                                            value = 'Laptops'
                                            onClick = { ( e ) => props.check ( e ) } />
                                            Laptops
                                    </p>
                                    <p className = 'product-item'>
                                        <input
                                            type = 'checkbox'
                                            name = { PRODUCTS }
                                            value = 'Mobiles'
                                            onClick = { ( e ) => props.check ( e ) } />
                                            Mobiles
                                    </p>
                                </div>
                                { error.productsError &&
                                    <Alert variant = 'danger' className = 'span'>
                                        { error.productsError }
                                    </Alert>
                                }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label'>Password</p>
                                <input
                                    type = 'password'
                                    name = { PASSWORD }
                                    value = { props.state.password }
                                    onChange = { ( e ) => props.change ( e ) }
                                    className = 'forminput'
                                    placeholder = 'Enter Password' />
                                { error.passwordError &&
                                    <Alert variant = 'danger' className = 'span'>
                                        { error.passwordError }
                                    </Alert>
                                }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className = 'label'>Confirm Password</p>
                                <input 
                                    type = 'password'
                                    name = { CONFIRM_PASSWORD }
                                    value = { props.state.confirmpass }
                                    onChange = { ( e ) => props.change ( e ) }
                                    className = 'forminput'
                                    placeholder = 'Re-Enter Password' />
                                { error.confirmpassError &&
                                    <Alert variant = 'danger' className = 'span'>
                                        { error.confirmpassError }
                                    </Alert>
                                }
                            </td>
                        </tr>
                        <tr>
                            <td >  
                                <Button 
                                    type = 'Submit'
                                    id = 'formbutton'
                                    onClick = { ( e ) => props.submit( e ) }
                                    disabled = { invalid } >
                                    Start
                                </Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </>
    );
}

export default SellerComponent;