import { Container, Table, Button } from 'react-bootstrap';

function CartComponent ( props ) {

    const { products } = props.state;
    return (
        <Container>
            <Table id='productstable'>
                <tbody>
                    <tr>
                        <td colSpan='2'>
                            <h2 id = 'productsheader'> Your Cart </h2>
                        </td>
                    </tr>
                    { products.map( ( product ) => 
                        <tr key = { product.id } >
                            <td> 
                                <img 
                                    alt = { product.name } 
                                    src = { product.ProductImage } />
                            </td>
                            <td>
                                <div className='productdetails'>
                                    <p> 
                                        <strong> Product Name </strong> :
                                        { product.name } 
                                    </p>
                                    <p> <strong>Price </strong> : Rs.
                                        { product.price }/-
                                    </p>
                                    <p> <strong>Brand </strong> :
                                        { product.brand }
                                    </p>
                                    <Button 
                                        variant = 'primary'
                                        onClick = { ( e ) => { props.remove( e, product ) } }>
                                        Remove
                                    </Button>
                                </div>
                            </td>
                        </tr>
                    )}
                    { products.length === 0 ?
                        <tr>
                            <td colSpan = '2' >
                                <h2 id = 'productsheader'>
                                    You're Have No Products in Cart 
                                </h2>
                            </td>
                        </tr> :
                        <tr>
                            <td>
                                <h2 id = 'productsheader'>
                                    Total Bill : RS. { props.state.bill } /- 
                                </h2>
                            </td>
                            <td>
                                <h3 id = 'productsheader'>
                                    <Button id = 'buy-now' onClick = { () => props.buynow() } >
                                        Buy Now
                                    </Button>
                                </h3>
                            </td>
                        </tr>
                    }
                </tbody>
            </Table>
        </Container>
    );
}

export default CartComponent;