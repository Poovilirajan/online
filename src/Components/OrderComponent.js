import { Container, Table, Button, Alert } from 'react-bootstrap';
import { CARD_NUMBER, CVV } from '../Utils/constants/inputNames'; 

function OrderComponent ( props ) {
    const { products } = props.state;
    const user = props.user;
    return (
        <Container >
            { props.state.success 
            && <div id = 'success'>
                <p id = 'success-message'>
                    Congratulations!<br/> Your Order was Confirmed!
                </p>
                <Button
                    variant = 'success'
                    id = 'success-button2'
                    onClick = { () => props.redirect( 'home' ) }>
                    Home
                </Button>
                <Button 
                    variant = 'success'
                    id = 'success-button2'
                    onClick = { () => props.redirect( 'orders' ) }>
                    Your Orders
                </Button>
            </div>
            }
            <Table className = 'orderdetail'>
                <tbody>
                    <tr>
                        <td colSpan = '2'>
                            <h2 id = 'productsheader'> Product Details </h2>
                        </td>
                    </tr>
                    { products.map( ( product, index ) => 
                        <tr key = { product.id} >
                            <td>
                                <img 
                                    src = { product.ProductImage }
                                    alt = { product.name } />
                            </td>
                            <td>
                                <div className='productdetails'>
                                    <p>
                                        <strong> Product Name </strong> :
                                        { product.name }
                                    </p>
                                    <p>
                                        <strong>Price </strong> : Rs.
                                        { product.price }/-
                                    </p>
                                    <p>
                                        <strong>Brand </strong> :
                                        { product.brand }
                                    </p>
                                    <p>
                                        <strong>Description </strong> :
                                        { product.desc }.
                                    </p>
                                    <div> 
                                        <p id = 'count-label'> 
                                            <strong>No. of Items </strong>   : 
                                        </p>
                                        <div id = 'count-buttons' >
                                            <button 
                                                className = 'count'
                                                onClick = { () => props.count( '-', product, index ) }>
                                                 - 
                                            </button>
                                            <p id = 'count-value'>
                                                { props.state.count[ index ] }
                                            </p>
                                            <button 
                                                className = 'count'
                                                onClick = { () => props.count( '+', product, index ) }>
                                                 + 
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr> )
                    }
                </tbody>
            </Table>
            <Table className = 'orderdetail'>
                <tbody>
                    <tr>
                        <td>
                            <h2 id = 'productsheader'> User Details </h2>
                        </td>
                        <td>
                            <h2 id = 'productsheader'> Payment Options </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div className = 'paymentdetails'>
                                <p>
                                    <strong> UserName </strong> :
                                    <br />
                                    { `${ user.firstname } ${ user.lastname }`}
                                </p>
                                <p>
                                    <strong> Address </strong>  :
                                    <br />
                                    { `${ user.firstname } ${ user.lastname },` }
                                    <br /> 
                                    { `${ user.addressline1 } ${ user.addressline2 }` }
                                    <br />
                                    { `${ user.city }, ` }
                                    <br />
                                    { `${ user.state } - ${ user.pincode }. ` }
                                </p>
                                <p>
                                    <strong> Mobile </strong> :
                                    <br />
                                    { user.mobile }
                                </p>
                            </div>
                        </td>
                        <td>
                            <div className = 'paymentdetails'>
                                <p>
                                    <strong> Choose Payment Type </strong> :
                                </p>
                                <p>
                                    <input 
                                        type = 'radio'
                                        name = 'payment'
                                        value = 'Credit Card'
                                        onChange = { ( e ) => props.payment( e ) } />
                                        Credit Card
                                </p>
                                { props.state.creditCardDetail &&
                                    <div className = 'cardinfo' >
                                        <p className = 'cardlabel'>Card Number </p>
                                        <input 
                                            name = { CARD_NUMBER }
                                            className = 'carddetail'
                                            placeholder = 'Enter Card Number'
                                            onChange = { (e) => props.change(e) } />
                                        { props.state.error.cardnumberError &&
                                            <Alert 
                                                variant = 'danger' 
                                                className = 'card' >
                                                { props.state.error.cardnumberError }
                                            </Alert>
                                        }
                                        <p className = 'cardlabel'>CVV</p>
                                        <input
                                            className = 'cvv'
                                            name = { CVV }
                                            placeholder = 'Enter CVV'
                                            onChange = { (e) => props.change(e) } />
                                        { props.state.error.cvvError &&
                                            <Alert
                                                variant = 'danger'
                                                className = 'card'>
                                                { props.state.error.cvvError }
                                            </Alert>
                                        } 
                                    </div>
                                }
                                <p>
                                    <input
                                        type = 'radio'
                                        name = 'payment'
                                        value = 'Debit Card'
                                        onChange = { ( e ) => props.payment( e ) }/>
                                        Debit Card
                                </p>
                                { props.state.debitCardDetail &&
                                    <div className = 'cardinfo' >
                                        <p className = 'cardlabel'>Card Number </p>
                                        <input
                                            name = { CARD_NUMBER }
                                            className = 'carddetail'
                                            placeholder = 'Enter Card Number'
                                            onChange = { (e) => props.change(e) } />
                                        { props.state.error.cardnumberError && 
                                            <Alert
                                                variant = 'danger'
                                                className = 'card' >
                                                { props.state.error.cardnumberError }
                                            </Alert> }
                                        <p className = 'cardlabel'>CVV</p>
                                        <input 
                                            className = 'cvv'
                                            name = { CVV }
                                            placeholder = 'Enter CVV'
                                            onChange = { (e) => props.change(e) } />
                                        { props.state.error.cvvError &&
                                            <Alert 
                                                variant = 'danger'
                                                className = 'card' >
                                                { props.state.error.cvvError }
                                            </Alert>
                                        } 
                                </div>}
                                <p>
                                    <input
                                        type = 'radio'
                                        name = 'payment'
                                        value = 'Cash On Delivery'
                                        onChange = { ( e ) => props.payment( e ) }/>
                                        Cash On Delivery
                                </p>
                                <p>
                                    <strong>Bill </strong> : Rs.
                                    { props.state.bill }/-
                                </p>
                                <Button
                                    variant = 'primary'
                                    onClick = { () => props.order( user, props.state.payment ) }
                                    disabled = { props.state.error.cardnumberError ||
                                        props.state.error.cvvError ||
                                        ! props.state.disabled } >
                                    Confirm Order
                                </Button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Container>
    );
}

export default OrderComponent;