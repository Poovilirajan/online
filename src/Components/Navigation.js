import { Link } from 'react-router-dom';

function Navigation ( props ) {
    const flag = JSON.parse( window.localStorage.getItem( 'isLogin' ) );

    return (
        <>
            { flag ?
                ( <>
                    <Link className = 'link1' to = '/your-orders' >Your Orders</Link>
                    <Link className = 'link1' to = '/cart' >Cart</Link>
                    <Link className = 'link1' to = '/' >Home</Link>
                </> ) :
                ( <>
                    <Link className = 'link1' to = '/seller'>Become a Seller</Link>
                    <Link className = 'link1' to = '/login' >Login</Link>
                    <Link className = 'link1' to = '/' >Home</Link>
                </> )
            }
        </>
    );
}

export default Navigation;