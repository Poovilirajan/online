import { Container, Table, Button } from 'react-bootstrap';

function OrderDetailComponent ( props ) {
    const { order } = props.state;
    const user = props.user;
    return (
        <Container >
            <Table className = 'orderdetail'>
                <tbody>
                    <tr>
                        <td>
                            <h2 id = 'productsheader'> User Details </h2>
                        </td>
                        <td>
                            <h2 id = 'productsheader'> Order Details </h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div className = 'paymentdetails'>
                                <p>
                                    <strong> UserName </strong> :
                                    <br />
                                    { `${ user.firstname } ${ user.lastname }`}
                                </p>
                                <p> 
                                    <strong> Billing Address </strong>  :
                                    <br />
                                    { `${ user.firstname } ${ user.lastname },` }
                                    <br />
                                    { `${ user.addressline1 } ${ user.addressline2 }` }
                                    <br />
                                    { `${ user.city }, ` }
                                    <br />
                                    { `${ user.state } - ${ user.pincode }. ` }
                                </p>
                                <p>
                                    <strong> Mobile </strong> :
                                    <br />
                                    { user.mobile }
                                </p>
                            </div>
                        </td>
                        <td>
                            <div className = 'paymentdetails'>
                                <p>
                                    <strong> Ordered On </strong> :
                                    <br/>
                                    { order.date.toDateString().slice(4) }
                                </p>
                                <p>
                                    <strong> Bill Amount </strong> :
                                    <br/> 
                                    Rs.{ order.bill }/-
                                </p>
                                <p>
                                    <strong> Payment Type </strong> :
                                    <br/>
                                    { order.payment }
                                </p>
                                <p>
                                    <strong>Order Status </strong> :
                                    <br/>
                                    { order.status }!
                                </p>
                                { order.card.cardnumber && 
                                    <p>
                                        <strong> Card Number </strong> :
                                        <br/>
                                        xxxx xxxx xxxx { order.card.cardnumber.slice(15) }
                                    </p>
                                }
                            </div>
                        </td>
                    </tr>
                    { order.status !== 'Cancelled' && 
                        <tr>
                            <td colSpan = '2'>
                                <h2 id = 'productsheader'> 
                                    <Button
                                        variant = 'primary'
                                        onClick = { () => props.cancel() }>
                                        Cancel Order
                                    </Button> 
                                </h2>
                            </td>
                        </tr>
                    }
                </tbody>
            </Table>
            <Table className = 'orderdetail'>
                <tbody>
                    <tr>
                        <td colSpan = '2'>
                            <h2 id = 'productsheader'> Product Details </h2>
                        </td>
                    </tr> 
                    <tr>
                        <td>
                            <img
                                src = { order.product.ProductImage }
                                alt = { order.product.name } />
                        </td>
                        <td>
                            <div className='productdetails'>
                                <p>
                                    <strong> Product Name </strong> :
                                    { order.product.name }
                                </p>
                                <p>
                                    <strong>Price </strong> : Rs.
                                    { order.product.price }/-
                                </p>
                                <p>
                                    <strong>Brand </strong> :
                                    { order.product.brand }
                                </p>
                                <p>
                                    <strong>Description </strong> :
                                    { order.product.desc }.
                                </p>
                                <p>
                                    <strong>Number of Items </strong> :
                                    { order.count }
                                </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </Table>
        </Container>
    );
}

export default OrderDetailComponent;