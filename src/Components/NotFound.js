import logo from '../Utils/images/notfound.jpg'

export default function NotFound( props ){
    return( <img id = 'logo' src = { logo } alt = 'Page Not found' /> );
}