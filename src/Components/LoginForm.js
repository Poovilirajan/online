import { Alert, Button } from 'react-bootstrap';
import { MOBILE, PASSWORD } from '../Utils/constants/inputNames'; 

function LoginForm ( props ) {

    return (
        <form>
            <table id = 'userForm'>
                <tbody>
                    <tr>
                        <td colSpan = '2' >
                            <p id = 'formtitle'> User Login </p>
                            { props.state.loginError && 
                                <Alert variant = 'danger' className = 'span'>
                                    Invalid Username or Password
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Mobile </p>
                            <input
                                name = { MOBILE }
                                value = { props.state.mobile }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput'
                                placeholder = 'Enter Mobile Number' />
                            { props.state.error.mobileError &&
                                <Alert variant = 'danger' className = 'span'>
                                    { props.state.error.mobileError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p className = 'label'>Password </p>
                            <input
                                type = 'password'
                                name = { PASSWORD }
                                value = { props.state.password }
                                onChange = { ( e ) => props.change( e ) }
                                className = 'forminput'
                                placeholder = 'Enter Password' />
                            { props.state.error.passwordError &&
                                <Alert variant = 'danger' className = 'span'>
                                    { props.state.error.passwordError }
                                </Alert>
                            }
                        </td>
                    </tr>
                    <tr>
                        <td colSpan = '2' >  
                            <Button
                                type = 'Submit'
                                id = 'formbutton'
                                onClick = { ( e ) => props.submit( e ) } 
                                disabled = { props.state.error.mobileError 
                                    || props.state.error.passwordError } >
                                Login
                            </Button>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan = '2' >
                            <p id = 'createaccount'>
                                <a href = '/create-account'>Create New Account</a>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    );
}

export default LoginForm;