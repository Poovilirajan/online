import { Container, Table, Button } from 'react-bootstrap';

function HomeComponent ( props ) {
    
    return ( 
        <Container>
            { props.state.showFilter &&
                <div id = 'filter'>
                    <Table id = 'filter-table'>
                        <thead>
                            <tr id = 'filter-Header' >
                                <td colSpan = '2'>
                                    <h5 id = 'productsheader' > Filters </h5>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h6 id = 'productsheader' > Price </h6>
                                </td>
                                <td>
                                    <h6 id = 'productsheader' > Category </h6>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <p id = 'productsheader' > 
                                        <input 
                                            type = 'checkbox'
                                            name = 'price'
                                            value = '10000-15000'
                                            checked = { props.state.priceFilter[ '10000-15000' ] }
                                            onChange = { ( e ) => props.change( e ) } />
                                            10000 - 15000 
                                    </p>
                                </td>
                                <td>
                                    <p id = 'productsheader' >
                                        <input 
                                            type = 'checkbox'
                                            name = 'category'
                                            value = 'Laptops'
                                            checked = { props.state.categoryFilter[ 'Laptops' ] }
                                            onChange = { ( e ) => props.change( e ) } />
                                            Laptops 
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p id = 'productsheader' >
                                        <input
                                            type = 'checkbox'
                                            name = 'price'
                                            value = '15000-20000'
                                            checked = { props.state.priceFilter[ '15000-20000' ] }
                                            onChange = { ( e ) => props.change( e ) } />
                                            15000 - 20000
                                    </p>
                                </td>
                                <td>
                                    <p id = 'productsheader' >
                                        <input
                                            type = 'checkbox'
                                            name = 'category'
                                            value = 'Mobiles'
                                            checked = { props.state.categoryFilter[ 'Mobiles' ] }
                                            onChange = { ( e ) => props.change( e ) } />
                                            Mobiles
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan = '2'>
                                    <center>
                                        <Button 
                                            onClick = { () => props.apply( false ) } >
                                            Remove
                                        </Button>  
                                        <Button 
                                            onClick = { () => props.apply( true ) } >
                                            Apply
                                        </Button>
                                    </center>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            }
            <Table id = 'productstable' >
                <tbody>
                    <tr>
                        <td colSpan = '2' >
                            <h2 id = 'productsheader'> Available Products </h2>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan='2'>
                            <Button
                                variant = 'primary'
                                className = 'filters'
                                onClick = { () => props.filters() } >
                                Filters
                            </Button>
                        </td>
                    </tr>
                    { props.products.map( ( product ) =>
                        <tr
                            key = { product.id }
                            onClick = { () => props.view( product ) } >
                            <td>
                                <img
                                    alt = { product.name }
                                    src = { product.ProductImage }/>
                            </td>
                            <td>
                                <div className='productdetails'>
                                    <p>
                                        <strong> Product Name </strong> :
                                        { product.name }
                                    </p>
                                    <p>
                                        <strong>Price </strong> : Rs.
                                        { product.price }/-
                                    </p>
                                    <p>
                                        <strong>Brand </strong> :
                                        { product.brand }
                                    </p>
                                    <Button
                                        variant = 'primary'
                                        onClick = { ( e ) => props.order( e, product ) }>
                                        Buy now
                                    </Button>
                                </div>
                            </td>
                        </tr> 
                    ) }
                </tbody>
            </Table>
        </Container>
    );
}

export default HomeComponent;