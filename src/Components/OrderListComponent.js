import { Container, Table, Button } from 'react-bootstrap';

function OrderListComponent ( props ) {
    const orders = props.orders;
    return (
        <Container>
            <Table id = 'orderstable'>
                <tbody>
                    <tr>
                        <td colSpan = '2' >
                            <h2 id = 'productsheader'> Your Orders </h2>
                        </td>
                    </tr>
                    { orders.map( ( order, index ) => 
                        <tr key = { index } >
                            <td>
                                <img
                                    alt={ order.product.name }
                                    src={ order.product.ProductImage } />
                            </td>
                            <td>
                                <div className='productdetails' >
                                    <p>
                                        <strong>Product Name </strong> :
                                        { order.product.name }
                                    </p>
                                    <p>
                                        <strong>Brand </strong> :
                                        { order.product.brand }
                                    </p>
                                    <p>
                                        <strong>Bill </strong> : Rs.
                                        { order.bill }/-
                                    </p>
                                    <p>
                                        <strong>Ordered On </strong> :
                                        { order.date.toDateString().slice(4) }
                                    </p>
                                    <p>
                                        <strong>Order Status </strong> :
                                        { order.status }!
                                    </p>
                                    <Button
                                        variant='primary'
                                        onClick = { ( e ) => { props.view ( index ) } }>
                                        View Details
                                    </Button>
                                </div>
                            </td>
                        </tr>
                    ) }
                    { orders.length === 0 &&
                        <tr>
                            <td colSpan = '2' >
                                <h2 id = 'productsheader'>
                                    You're Have No Orders Till Now
                                </h2>
                            </td>
                        </tr>
                    }
                </tbody>
            </Table>
        </Container>
    );
}

export default OrderListComponent;