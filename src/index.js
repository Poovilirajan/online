import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import generateStore from './Store/configureStore';
import middlewares from './Middlewares';
import rootReducer from './Reducers/rootReducer'

// It creates a store by using array contains list of middlewares and a reducer.
const store = generateStore( rootReducer, middlewares );

// It sets login flag false.  
window.localStorage.setItem ( 'isLogin', false );

ReactDOM.render(
    <Provider store = { store }>
      <App />
    </Provider>,
    document.getElementById( 'root' )
);