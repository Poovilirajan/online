import { CREATE_USER, CHANGE_USER, GET_PRODUCTS, ADD_ORDER, ADD_TO_CART, REMOVE_FROM_CART, EDIT_ORDER, CREATE_SELLER, LOG_OUT, PRODUCT_DETAILS } from '../Action/actionTypes';

const initialstate = {
    user:{
        "firstname": "Patrick",
        "lastname": "Wayne",
        "mobile": "9876543210",
        "password": "Wayneway",
        "addressline1": "111, new old first street",
        "addressline2": "",
        "state": "Tamil Nadu",
        "city": "Madurai",
        "pincode": "123456",
        "orders": [],
        "cart": []
      },
    products: undefined,
    productDetail: undefined,
    loggedUser: undefined,
    seller: undefined
};

export const userReducer = ( state = initialstate, action ) => {
    switch( action.type ) {
        case CREATE_USER:
            return {
                ...state,
                user: action.user
            };
        case CREATE_SELLER:
            return {
                ...state,
                seller: [action.user]
            };

        case CHANGE_USER:
            if ( state.user.mobile === action.user.mobile &&
                state.user.password === action.user.password ) {
                window.localStorage.setItem( 'isLogin', true );
                return {
                    ...state,
                    loggedUser: state.user
                };
            } else {
                return state;
            }

        case LOG_OUT:
            window.localStorage.setItem( 'isLogin', false );
            return {
                ...state,
                loggedUser: undefined
            };

        case GET_PRODUCTS:
            return {
                ...state,
                products: action.products
            };
        
        case PRODUCT_DETAILS:
            return {
                ...state,
                productDetail: action.product
            };

        case ADD_ORDER:
            return {
                ...state,
                products: undefined,
                user: action.user, 
                loggedUser: action.user,
            };

        case EDIT_ORDER:
            return {
                ...state,
                user: action.user,
                loggedUser: action.user,
            };

        case ADD_TO_CART:
            return {
                ...state,
                user: action.user,
                loggedUser: action.user,
            };

        case REMOVE_FROM_CART:
            return {
                ...state,
                user: action.user,
                loggedUser: action.user,
            };

        default:
            return state;

    }

}