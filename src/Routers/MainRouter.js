import { Switch, Route, Router } from 'react-router-dom';
import Home from '../Containers/Home';
import Login from '../Containers/Login';
import CreateAccount from '../Containers/CreateAccount';
import NotFound from '../Components/NotFound';
import Detail from '../Containers/Detail';
import Order from '../Containers/Order';
import OrderList from '../Containers/OrderList';
import OrderDetail from '../Containers/OrderDetail';
import Cart from '../Containers/Cart';
import History from '../Utils/history';
import Seller from '../Containers/Seller';
import Navigation from '../Components/Navigation'
import PrivateRoute from './PrivateRoute';

// MainRouter used to navigate across the application.
function MainRouter( props ){

    return( 
        <Router history = { History }>
            <Navigation />  
            <Switch>
                <Route exact path = '/' component = { Home }/>
                <Route path = '/login' component = { Login }/>
                <Route path = '/create-account' component = { CreateAccount }/>
                <Route path = '/detail/' component = { Detail } />
                <PrivateRoute path = '/order' component = { Order } />
                <PrivateRoute path = '/your-orders' component = { OrderList } />
                <PrivateRoute path = '/order-detail/:id' component = { OrderDetail }/>
                <PrivateRoute path = '/cart' component = { Cart } />
                <Route path = '/seller' component = { Seller } />
                <Route component = { NotFound }/>
            </Switch>
        </Router> 
    );
}

export default MainRouter;