import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import React from 'react';
import MainRouter from './Routers/MainRouter';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import History from './Utils/history';
import { logOutUser } from './Action/userActions';
import { bindActionCreators } from 'redux';

class App extends React.Component {

    constructor( props ) {
        super( props );
        this.state = {};
    }

    // Handles logout action
    handleLogout = () => {
        this.props.logOutUser();
        History.push( '/' );
    }

    render() {
        return( 
            <div className = "App">
                <h1 className = 'headertitle'> E - ZONE </h1>
                { this.props.logUser &&
                    <Button variant = 'primary' id = 'logout' 
                        onClick = { () => this.handleLogout() }> Logout </Button>
                }
                <MainRouter />
            </div>
        );
    }

}

function mapStateToProps( state ) {
    return {
        logUser: state.userReducer.loggedUser,
    };
}

function mapDispatchToProps( dispatch ) {

    return bindActionCreators( { logOutUser }, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( App );