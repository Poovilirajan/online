import React from 'react';
import { connect } from 'react-redux';
import OrderListComponent from '../Components/OrderListComponent';
import ReactPaginate from 'react-paginate';
import { Container } from 'react-bootstrap';
import { ORDER_LIST } from '../Utils/constants/initialData';

class OrderList extends React.Component {
    constructor ( props ) {
        super ( props );
        this.state = ORDER_LIST;
    }

    fetchData = () => {
        const count = Math.ceil( this.props.logUser.orders.length / this.state.perPage );
        const pageData = this.props.logUser.orders.slice( this.state.offset,
            this.state.offset + this.state.perPage );
        this.setState ( { pageData, pageCount: count } );
    }

    componentDidMount(){
        if( this.props.logUser ){
            this.fetchData();
        }
    }

    handleView = ( index ) => {
        this.props.history.push( `/order-detail/${ index }` );
    }

    handleChangePage = ( e ) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;
        this.setState( { currentPage:selectedPage, offset },
            () => this.fetchData() );
    }

    render () {

        return (
            <div> 
                <OrderListComponent
                    orders = { this.state.pageData }
                    view = { this.handleView }
                />
                <Container>
                    <div id = 'footer' >
                        <ReactPaginate
                            previousLabel = {"prev" }
                            nextLabel = { "next" }
                            breakLabel = { "..." }
                            breakClassName = { "break-me" }
                            pageCount = { this.state.pageCount }
                            marginPagesDisplayed = { 1 }
                            pageRangeDisplayed = { 3 }
                            onPageChange = { this.handlePageChange }
                            containerClassName = { "pagination" }
                            subContainerClassName = { "pages pagination" }
                            activeClassName = { "active" }
                        />
                    </div>
                </Container>
            </div>
        );
    }
}

function mapStateToProps( state ) {
    return {
        logUser: state.userReducer.loggedUser
    };
}

export default connect( mapStateToProps )( OrderList );