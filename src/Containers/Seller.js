import React from 'react';
import { connect } from 'react-redux';
import { createNewSeller } from '../Action/userActions';
import SellerComponent from '../Components/SellerComponent';
import { validate } from '../Utils/validation';
import { bindActionCreators } from 'redux';
import { SELLER } from '../Utils/constants/initialData';

class Seller extends React.Component {
    constructor ( props ) {
        super ( props );
        this.state = SELLER;
    }

    handleChange = ( { target } = {} ) => {
        if ( target ) {
            const { name, value } = target;
            let errors;
            if ( name === 'confirmpass') {
                const exp=new RegExp( '^'+this.state.password+'$' );
                errors = validate( name, value, this.state.error, exp );
            }
            else {
                errors = validate ( name, value, this.state.error );
            }
            this.setState( { [name]: value, error: errors.error } );
        }
    }

    handleCheck = ( { target } = {} ) => {
        let { products } = this.state;
        if( target ) {
            if ( target.checked ) {
                products = [ ...products, target.value ]
            }
            else {
                products = products.filter ( ( product ) => product !== target.value );
            }
            const errors = validate ( target.name, products, this.state.error );
            this.setState ( { products, error: errors.error } );
        }
    }

    handleSubmit = ( e ) => {
        e.preventDefault();
        this.props.createNewSeller( this.state );
        this.setState ( { success: true } );
    }

    handleRedirect = () => {
        this.props.history.push ( '/' );
    }

    render () {
        
        return (
            <SellerComponent 
                state = { this.state }
                change = { this.handleChange }
                check = { this.handleCheck }
                submit = { this.handleSubmit }
                redirect = { this.handleRedirect }
            />
        );
    }
}

function mapDispatchToProps( dispatch ) {

    return bindActionCreators( { createNewSeller }, dispatch );
}

export default connect( null, mapDispatchToProps )( Seller );