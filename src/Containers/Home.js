import React from 'react';
import Loader from '../Utils/images/plane.gif';
import { connect } from 'react-redux';
import { getAllProducts } from '../Action/productActions';
import HomeComponent from '../Components/HomeComponent';
import { getProducts, productDetails } from '../Action/productActions';
import ReactPaginate from 'react-paginate';
import { Container } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { getFilters, apply } from '../Utils/filters';
import { HOME } from '../Utils/constants/initialData';

class Home extends React.Component{

    constructor( props ){
        super( props );
        this.state = HOME;
    }

    componentDidMount() {
        getAllProducts()
        .then( ( res ) => {
            const count = Math.ceil( res.data.products.length / this.state.perPage );
            this.setState(
                { 
                    products: res.data.products,
                    pageCount: count 
                },
                () => this.fetchData()
            );
        } )
        .catch( ( err ) => {
            console.log( err );
        } );
    }

    showFilter = () => {
        this.setState( { showFilter: true } );
    }

    applyFilter = ( filterFlag ) => {
        let products = [];
        let count, flag;
        let { categoryFilter, priceFilter, categories, low, high } = this.state;
        if ( filterFlag ) {
            products = apply( this.state );
            count = Math.ceil( products.length/this.state.perPage );
            flag = true;
        } else {
            count = Math.ceil( this.state.products.length/this.state.perPage );
            ( { categoryFilter, priceFilter, categories, low, high } = HOME );
            flag = false;
        }
        this.setState (
            {
                isFilter: flag,
                showFilter: false,
                filteredProducts: products,
                priceFilter,
                categoryFilter,
                pageCount: count,
                offset: 0,
                currentPage: 0,
                high,
                low,
                categories
            },
            () => this.fetchData()
        );
    }

    handleChange = ( { target } = {} ) => {
        if ( target ) {
            const filters = getFilters ( target, this.state ) ;
            this.setState( {
                categoryFilter: filters.categoryFilter,
                categories: filters.categories,
                priceFilter: filters.priceFilter,
                low: filters.tempLow,
                high: filters.tempHigh
            } );
        }
    }

    handleOrder = ( e, product ) => {
        e.stopPropagation();
        if ( ! this.props.logUser ) {
            this.props.history.push( '/login' );
        } else {
            this.props.getProducts( [ product ] );
            this.props.history.push( `/order` );
        }
    }

    handleView = ( product ) => {
        this.props.productDetails( product );
        this.props.history.push( `/detail`);
    }

    handlePageChange = ( e ) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;
        this.setState( { currentPage: selectedPage, offset }, () => this.fetchData() );
    }

    fetchData = () => {
        const pageData = ( this.state.isFilter ?
            this.state.filteredProducts :
            this.state.products )
        .slice( this.state.offset, this.state.offset + this.state.perPage );
        this.setState( { pageData } );
    }

    render() {

        return (
            <div>
                { this.state.products.length === 0 ?
                    ( <div id = 'loader' >
                        <img src = { Loader } alt = 'Loader'/> 
                    </div> ) :
                    ( <>
                        <HomeComponent
                            products = { this.state.pageData }
                            filters = { this.showFilter }
                            order = { this.handleOrder }
                            view = { this.handleView }
                            state = { this.state }
                            change = { this.handleChange }
                            apply = {this.applyFilter }
                        />
                        <Container>
                            <div id = 'footer' >
                                <ReactPaginate
                                    previousLabel = {"prev" }
                                    nextLabel = { "next" }
                                    breakLabel = { "..." }
                                    breakClassName = { "break-me" }
                                    pageCount = { this.state.pageCount }
                                    marginPagesDisplayed = { 1 }
                                    pageRangeDisplayed = { 3 }
                                    onPageChange = { this.handlePageChange }
                                    containerClassName = { "pagination" }
                                    subContainerClassName = { "pages pagination" }
                                    activeClassName = { "active" }
                                />
                            </div>
                        </Container>
                    </>)
                }
            </div>
        );
        
    }

}

function mapStateToProps( state ) {
    return {
        products: state.userReducer.products,
        logUser: state.userReducer.loggedUser
    };
}

function mapDispatchToProps( dispatch ) {

    return bindActionCreators( { getProducts, productDetails }, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( Home );