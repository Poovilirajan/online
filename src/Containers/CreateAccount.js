import React from 'react';
import { connect } from 'react-redux';
import { createUser } from '../Action/userActions';
import CreateAccountForm from '../Components/CreateAccountForm';
import { validate } from '../Utils/validation';
import { bindActionCreators } from 'redux';
import { CREATE_ACCOUNT } from '../Utils/constants/initialData';

class CreateAccount extends React.Component{

    constructor( props ) {
        super( props );
        this.state = CREATE_ACCOUNT;
    }

    handleChange = ( { target } = {} ) => {
        if ( target ) {
            const { name, value } = target;
            let change;
            if ( name === 'confirmpass') {
                const exp=new RegExp( '^'+this.state.password+'$' );
                change = validate( name, value, this.state.error, exp );
            } else {
                change = validate( name, value, this.state.error );
            }
            this.setState( { [name]: value, error: change.error } );
        }
    }

    handleSubmit = ( e ) => {
        e.preventDefault();
        this.props.createUser( this.state );
        this.props.history.push('/login');
    }

    render(){        
        return ( 
            <CreateAccountForm
                state = { this.state }
                change = { this.handleChange }
                submit = { this.handleSubmit }
            />
        );
    }

}

function mapDispatchToProps( dispatch ){
    return bindActionCreators( { createUser }, dispatch );
}

export default connect( null, mapDispatchToProps )( CreateAccount );