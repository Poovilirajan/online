import React from 'react';
import { connect } from 'react-redux';
import { addOrder } from '../Action/productActions';
import { validate } from '../Utils/validation';
import OrderComponent from '../Components/OrderComponent';
import { CARD_NUMBER } from '../Utils/constants/inputNames';
import { bindActionCreators } from 'redux';
import { ORDER } from '../Utils/constants/initialData';

class Order extends React.Component {
    constructor( props ) {
        super( props );
        this.state = ORDER;
    }

    componentDidMount () {
        const { products } = this.props;
        let price = 0;
        const count = products.map ( ( product ) => {
            price += Number( product.price );
            return 1;
        } );
        this.setState( { products, bill: price, count } );
    }

    handlePayment = ( { target } = {} ) => {
        if ( target ) {
            switch ( target.value ) {
                case 'Credit Card':
                    this.setState ( {
                        payment: target.value,
                        debitCardDetail: false,
                        creditCardDetail: true,
                        error: { cardnumberError: ' ', cvvError: ' ' }
                    } );
                    break;
                case 'Debit Card':
                    this.setState ( { 
                        payment: target.value,
                        creditCardDetail: false,
                        debitCardDetail: true,
                        error: { cardnumberError: ' ', cvvError: ' ' }
                    } );
                    break;
                case 'Cash On Delivery':
                    this.setState ( { 
                        payment: target.value,
                        debitCardDetail: undefined,
                        creditCardDetail: undefined,
                        cardNumber: undefined,
                        cvv: undefined,
                        error: { cardnumberError: '', cvvError: '' }
                    } );
                    break;
                default:
                    break;
            }
        }
    }

    handleChange = ( { target } = {} ) => {
        if ( target ) {
            if ( target.name === CARD_NUMBER ) {
                const length = target.value.length;
                if ( length === 4 || length === 9 || length === 14 ) {
                    target.value += ' ';
                }
            }
            const { name, value } = target;
            const change = validate( name, value, this.state.error );
            this.setState( { [name]: value, error: change.error } );
        }
    }

    handleCount = ( type, product, index ) => {
        let count = this.state.count;
        let value = count[ index ];
        let price = this.state.bill;
        if ( value !== 0 ){
            value = type === '+' ? value + 1 : value - 1;
            price = type === '+' ?
            price + Number( product.price ) :
            price - Number( product.price );
        } else {
            value = type === '+' ? value + 1 : value ;
            price = type === '+' ? price + Number( product.price ) : price ;
        }
        count[ index ] = value;
        this.setState ( { count, bill: price, disabled: value === 0 ? false : true } );
    }

    handleOrder = ( user, payment ) => {
        const date = new Date();
        let orders = [];
        const { cardnumber, cvv } = this.state;
        const card = {
            cardnumber,
            cvv
        };
        orders = this.state.products.map ( ( product, index ) => {
            return {
                product,
                payment,
                date,
                card,
                status: 'Placed',
                count: this.state.count[ index ],
                bill: product.price * this.state.count[ index ]
            };
        } );
        let path;
        if( this.props.location.state ){
            path = this.props.location.state.sourcePath;
        }
        this.props.addOrder( orders, user, path );
        this.setState( { success: true } );
    }

    handleRedirect = ( path ) => {
        if ( path === 'home' ) {
            this.props.history.push( '/' );
        }
        else if ( path === 'orders' ) {
            this.props.history.push( '/your-orders' );
        }
    }

    render() {

        if ( ! this.state.products ) {
            return <h1>Loading...</h1>;
        }

        return (
            <OrderComponent 
                state = { this.state }
                user = { this.props.logUser }
                order = { this.handleOrder }
                change = { this.handleChange }
                payment = { this.handlePayment }
                redirect = { this.handleRedirect }
                count = { this.handleCount }
            />
        );

    }

}

function mapStateToProps( state ) {
    return {
        products:state.userReducer.products,
        logUser: state.userReducer.loggedUser
    };
}

function mapDispatchToProps( dispatch ) {

    return bindActionCreators( { addOrder }, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( Order );