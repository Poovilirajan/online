import React from 'react';
import { removeProductFromCart, getProducts } from '../Action/productActions';
import { connect } from 'react-redux';
import CartComponent from '../Components/CartComponent';
import { bindActionCreators } from 'redux';
import { CART } from '../Utils/constants/initialData';

class Cart extends React.Component {
    constructor( props ) {
        super( props );
        this.state = CART;
    }

    componentDidMount(){
        if ( this.props.logUser ) {
            let bill = 0;
            const products = this.props.logUser.cart.map( ( product ) => {
                bill += Number( product.price );
                return product;
            });
            this.setState( { products, bill } );
        }
    }

    handleRemove = ( e, product ) => {
        e.stopPropagation();
        const bill = this.state.bill - product.price;
        this.props.removeProductFromCart( product, this.props.logUser );
        this.setState( { products: this.props.logUser.cart, bill } );
    }

    handlebuyNow = () => {
        if ( ! this.props.logUser ) {
            const state = `/cart`;
            this.props.history.push( '/login', state );
        } else {
            const state = { sourcePath: `/cart` };
            this.props.getProducts( this.state.products );
            this.props.history.push( `/order`, state );
        }
    }

    render(){
        
        return (
            <CartComponent 
                state = { this.state }
                remove = { this.handleRemove }
                buynow = { this.handlebuyNow }
            />
        );
    }

}

function mapStateToProps( state ) {
    return {
        logUser: state.userReducer.loggedUser,
    }
}

function mapDispatchToProps( dispatch ) {
    return bindActionCreators( { removeProductFromCart, getProducts }, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( Cart );