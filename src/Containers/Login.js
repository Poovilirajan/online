import React from 'react';
import { connect } from 'react-redux';
import { changeUser } from "../Action/userActions";
import LoginForm from '../Components/LoginForm';
import { validate } from '../Utils/validation';
import { bindActionCreators } from 'redux';
import { LOGIN } from '../Utils/constants/initialData';

class Login extends React.Component{

    constructor( props ){
        super( props );
        this.state = LOGIN;
    }

    handleChange = ( { target } = {} ) => {
        if( target ){
            const { name, value } = target;
            const change = validate( name, value, this.state.error );
            this.setState( { [name]: value, error: change.error } );
        }
    }

    handleSubmit = ( e ) => {
        e.preventDefault();

        const user = {
            mobile:this.state.mobile,
            password:this.state.password
        };

        const flag = this.props.user.mobile === user.mobile &&
            this.props.user.password === user.password ;

        if ( flag ) {
            this.props.changeUser( user );
            this.setState( { isLogin: true } );
            const path = this.props.location.state ? this.props.location.state : '/' ;
            this.props.history.push( path );
        } else {
            this.setState( { loginError : true }  );
        }
    }

    componentDidMount() {
        if ( this.props.logUser ) {
            this.setState( { isLogin: true } );
        }
    }
    
    render() {
        
        if ( this.state.isLogin ) {
            return <p> you're already logged in </p>;
        }

        return (
            <LoginForm
                state = { this.state }
                change = { this.handleChange }
                submit = { this.handleSubmit }
            />
        );
        
    }

}

function mapStateToProps( state ) {
    return {
        user:state.userReducer.user,
        logUser:state.userReducer.loggedUser
    };
}

function mapDispatchToProps( dispatch ) {

    return bindActionCreators( { changeUser }, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( Login );