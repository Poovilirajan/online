import React from 'react';
import { connect } from 'react-redux';
import OrderDetailComponent from '../Components/OrderDetailComponent';
import { editOrder } from '../Action/productActions';
import { bindActionCreators } from 'redux';
import { ORDER_DETAIL } from '../Utils/constants/initialData';

class OrderDetail extends React.Component {
    constructor ( props ) {
        super ( props );
        this.state = ORDER_DETAIL;
    }

    componentDidMount(){
        if ( this.props.match.params.id ) {
            const id = this.props.match.params.id;
            const order = this.props.logUser.orders[ id ];
            this.setState( { order } );
        }
    }

    handleCancel = () => {
        let { order } = this.state;
        order.status = 'Cancelled';
        this.props.editOrder( order, this.props.logUser, this.props.match.params.id );
        this.props.history.push ( '/your-orders' );
    }

    render() {

        if ( ! this.state.order ){
            return <h1> No Order </h1>;
        }
        
        return (
            <OrderDetailComponent 
                state = { this.state }
                user = { this.props.logUser }
                cancel = { this.handleCancel }
            />
        );
        
    }
    
}

function mapStateToProps( state ) {
    return {
        logUser: state.userReducer.loggedUser,
    }
}

function mapDispatchToProps( dispatch ) {
    return bindActionCreators( { editOrder }, dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( OrderDetail );