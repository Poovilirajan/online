import React from 'react';
import { connect } from 'react-redux';
import { addProductToCart, getProducts } from '../Action/productActions';
import DetailComponent from '../Components/DetailComponent';
import { bindActionCreators } from 'redux';
import { DETAIL } from '../Utils/constants/initialData';

class Detail extends React.Component{
    constructor( props ) {
        super( props );
        this.state = DETAIL;
    }

    componentDidMount() {
        const product = this.props.product;
        this.setState( { product } );
    }

    handleCart = ( product ) => {
        if( ! this.props.logUser ) {
            this.props.history.push( '/login' );
        } else {
            this.props.addProductToCart( product, this.props.logUser );
            this.setState( { cartSuccess: true } );
            this.props.history.push( '/cart' );
        }
    }

    handleOrder = ( product ) => {
        if ( ! this.props.logUser ) {
            this.props.history.push( '/login' );
        } else {
            this.props.getProducts( [ product ] );
            this.props.history.push( `/order` );
        }
    }

    render() {
        
        if ( ! this.state.product ) {
            return <h1>loading....</h1>;
        }

        return (
            <DetailComponent
                state = { this.state }
                cart = { this.handleCart }
                order = { this.handleOrder }
            />
        );
    }

}

function mapStateToProps( state ) {

    return {
        product:state.userReducer.productDetail,
        logUser:state.userReducer.loggedUser
    };
}

function mapDispatchToProps( dispatch ) {

    return bindActionCreators( { getProducts, addProductToCart } , dispatch );
}

export default connect( mapStateToProps, mapDispatchToProps )( Detail );