import { REMOVE_FROM_CART } from "../Action/actionTypes";
import API from '../Axios';

const removeFromCart = ( store ) => ( next ) => ( action ) => {
    if ( action.type === REMOVE_FROM_CART ) {
        API.post( '65e99db4-0f74-4939-ba52-6f684f3a4b72', action.product )
        .then( ( res ) => {
            next( action );
        })
        .catch( ( err ) => {
            console.log( err );
        } );

    } else {
        next( action );
    }
} 
export default removeFromCart;