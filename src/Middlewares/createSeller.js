import { CREATE_SELLER } from "../Action/actionTypes";
import API from '../Axios';

const createSeller = ( store ) => ( next ) => ( action ) => {
    if ( action.type === CREATE_SELLER ) {
        API.post( '75ac98cc-5e2c-4363-b5c4-553af276f644', action.user )
        .then( ( res ) => {
            next( action );
        } )
        .catch( (err) =>{
            console.log( err );
        } );

    } else {
        next( action );
    }
}

export default createSeller;