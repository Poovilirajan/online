import { EDIT_ORDER } from "../Action/actionTypes";
import API from '../Axios';

const editOrder = ( store ) => ( next ) => ( action ) => {
    if ( action.type === EDIT_ORDER ) {
        API.post( 'a3701042-7981-45bc-a92e-c58b2120d32f', action.userOrder )
        .then( ( res ) => {
            next( action );
        })
        .catch( ( err ) => {
            console.log( err );
        } );

    } else {
        next( action );
    }
} 

export default editOrder;