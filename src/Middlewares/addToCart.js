import { ADD_TO_CART } from "../Action/actionTypes";
import API from '../Axios';

const addToCart = ( store ) => ( next ) => ( action ) => {
    if ( action.type === ADD_TO_CART ) {
        API.post( 'ee5e53cc-4b97-4c57-9fd0-d2b6ea3f097c', action.product )
        .then( ( res ) => {
            next ( action );
        })
        .catch( ( err ) => {
            console.log( err );
        } );

    } else {
        next( action );
    }
}

export default addToCart;