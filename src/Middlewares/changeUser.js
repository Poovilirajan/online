import { CHANGE_USER } from "../Action/actionTypes";
import API from '../Axios';

const changeUser = ( store ) => ( next ) => ( action ) => {
    if ( action.type === CHANGE_USER ) {
        API.post( '55ee0a54-a4ba-4ca2-8c44-9b8ac7202e2d', action.user )
        .then( ( res ) => {
            next( action );
        })
        .catch( ( err ) => {
            console.log( err );
        } );

    } else {
        next( action );
    }
}

export default changeUser;