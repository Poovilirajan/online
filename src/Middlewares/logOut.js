import { LOG_OUT } from "../Action/actionTypes";
import API from '../Axios';

const logOut = ( store ) => ( next ) => ( action ) => {
    if ( action.type === LOG_OUT ) {
        API.get( '8759aaf8-cc64-4db3-ae6b-abc536e38ae5' )
        .then( ( res ) => {
            next( action );
        })
        .catch( ( err ) => {
            console.log( err );
        } );

    } else {
        next( action );
    }
}

export default logOut;