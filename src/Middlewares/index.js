import addOrder from "./addOrder";
import addToCart from "./addToCart";
import changeUser from "./changeUser";
import createSeller from "./createSeller";
import createUser from "./createUser";
import editOrder from "./editOrder";
import logOut from "./logOut";
import removeFromCart from "./removeFromCart";

const middlewares = [ addOrder, addToCart, changeUser, createSeller, createUser, editOrder, logOut, removeFromCart ];

export default middlewares;