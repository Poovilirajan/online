import { CREATE_USER } from "../Action/actionTypes";
import axios from "axios";

const createUser = ( store ) => ( next ) => ( action ) => {
    if ( action.type === CREATE_USER ) {
        axios.post( 'https://run.mocky.io/v3/a8d5d56a-3a53-4bd1-a8dc-e1e0b0a83978',
            action.user )
        .then( ( res ) => {
            action.user = res.data;
            next( action );
        } )
        .catch( (err) =>{
            console.log( err );
        } );

    } else {
        next( action );
    }
}

export default createUser;