import { ADD_ORDER } from "../Action/actionTypes";
import API from '../Axios';

const addOrder = ( store ) => ( next ) => ( action ) => {
    if ( action.type === ADD_ORDER ) {
        API.post( '1c9862df-1a51-4b24-aaab-4cdd3393c007', action.order )
        .then( ( res ) => {
            next( action );
        })
        .catch( ( err ) => {
            console.log( err );
        });

    } else {
        next( action );
    }
}

export default addOrder;